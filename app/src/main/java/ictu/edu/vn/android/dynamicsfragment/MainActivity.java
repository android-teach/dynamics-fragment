package ictu.edu.vn.android.dynamicsfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnShowFragment).setOnClickListener(this);
        findViewById(R.id.btnShowFragmentWithBack).setOnClickListener(this);
         // test
        showFragment(new BlueFragment(), false);
    }

    /** show fragment into layoutFragmnet
     * @param isAddToBackStack = true -> we can click back button to come back old fragment
     *                         = false -> we can not come back old fragment
     */
    private void showFragment(Fragment fragment, boolean isAddToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.replace(R.id.layoutFragment, fragment);

        if (isAddToBackStack) {
            ft.addToBackStack(null);
        }

        ft.commit();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnShowFragment) showFragment(new RedFragment(), false);
        else showFragment(new RedFragment(), true);
    }
}

